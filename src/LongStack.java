import java.util.Arrays;
import java.util.LinkedList;

public class LongStack {

   private LinkedList<Long> stack;

   public static void main (String[] argum) {
      System.out.println(interpret("5 3 4 ROT - +"));

   }

   LongStack() {
      stack = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clonedStack = new LongStack();
      for (Long l: stack) {
         clonedStack.push(l);
      }
      return clonedStack;
   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push (long a) {
      stack.addLast(a);
   }

   public long pop() {
      if (stack.size() == 0) {
         throw new RuntimeException("Not enough numbers");
      }
      long l = stack.getLast();
      stack.removeLast();
      return l;
   }

   public void op (String s) {
      if (!("+-*/".contains(s)) && !s.equals("SWAP") && !s.equals("ROT")) {
         throw new RuntimeException("Operation " + s + " not allowed");
      }
      if (s.equals("ROT")) {
         if (stack.size() < 3) {
            throw new RuntimeException("Cannot perform");

         }
         long op3 = pop();
         long op2 = pop();
         long op1 = pop();
         push(op2);
         push(op3);
         push(op1);

      } else if (stack.size() >= 2) {
         long op2 = pop();
         long op1 = pop();
         if (s.equals("*")) {
            push(op1 * op2);
         } else if (s.equals("/")) {
            push(op1 / op2);
         } else if (s.equals("+")) {
            push(op1 + op2);
         } else if (s.equals("-")) {
            push(op1 - op2);
         } else if (s.equals("SWAP")) {
            push(op2);
            push(op1);
         }
      } else {
         throw new RuntimeException("Cannot perform " + s + " operation");
      }
   }

   public long tos() {
      if (stack.size() == 0) {
         throw new RuntimeException("Not enough numbers");
      }
      return stack.getLast();
   }

   @Override
   public boolean equals (Object o) {
      if (this.getClass().equals(o.getClass())) {
         LongStack stack2 = (LongStack) o;
         if (this.getSize() == stack2.getSize()) {
            for (int i = 0; i < getSize(); i++) {
               if (!(this.getElement(i) == stack2.getElement(i))) {
                  return false;
               }
            }
            return true;
         }
      }
      return false;
   }

   public long getElement(int index) {
      return stack.get(index);
   }

   public int getSize() {
      return stack.size();
   }

   @Override
   public String toString() {
      StringBuffer stringBuffer = new StringBuffer();
      for (Long l : stack) {
         stringBuffer.append(l + " ");
      }
      return stringBuffer.toString();
   }


   // https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
   public static long interpret (String pol) {
      if (pol.length() == 0) {
         throw new RuntimeException("Empty expression");
      }

      LongStack longStack = new LongStack();
      String str = pol.trim().replaceAll("\\s+", " ");
      LinkedList<String> list = new LinkedList(Arrays.asList(str.split(" ")));
      String operators = "+-*/";

      for (String s : list) {


//         if (!operators.contains(s)) {
//            try {
//               longStack.push(Long.parseLong(s));
//            } catch (NumberFormatException e) {
//               throw new RuntimeException("Illegal symbol " + s + " in expression " + pol);
//            }
//         } else if (operators.contains(s) || s.equals("SWAP")){
//            try {
//               longStack.op(s);
//            } catch (RuntimeException e) {
//               throw new RuntimeException("Cannot perform " + s + " in expression " + pol);
//            }
//         }

         if (operators.contains(s) || s.equals("SWAP") || s.equals("ROT")) {

            try {
               longStack.op(s);
            } catch (RuntimeException e) {
               throw new RuntimeException("Cannot perform " + s + " in expression " + pol);
            }

         } else {
            try {
               longStack.push(Long.parseLong(s));
            } catch (NumberFormatException e) {
               throw new RuntimeException("Illegal symbol " + s + " in expression " + pol);
            }
         }
      }
      if (longStack.getSize() > 1) {
         throw new RuntimeException("Too many numbers in expression " + pol);
      }
      return longStack.tos();
   }

}
